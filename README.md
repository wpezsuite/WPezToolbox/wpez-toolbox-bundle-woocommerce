## WPezToolbox - Bundle: WooCommerce

__WC - A WPezToolbox bundle of WordPress WooCommerce plugins.__

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Important

The plugins contained in WPezToolbox bundles are simply "you might consider these suggestions." They are not formal endorsements; there is no claim being made about their quality and/or functionality. Also, some links might be affiliate links. 


### Overview

See the README here: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### This Bundle Includes

- https://wordpress.org/plugins/woocommerce/

- https://wordpress.org/plugins/woocommerce-eu-vat-compliance/

- https://wordpress.org/plugins/woocommerce-direct-checkout/

- https://wordpress.org/plugins/woocommerce-jetpack/

- https://wordpress.org/plugins/rafflepress/

- https://wordpress.org/plugins/yith-woocommerce-ajax-search/

- https://wordpress.org/plugins/woocommerce-currency-switcher/

- https://wordpress.org/plugins/woocommerce-menu-bar-cart/

- https://wordpress.org/plugins/woocommerce-multilingual/

- https://wordpress.org/plugins/woocommerce-products-filter/

- https://en-ca.wordpress.org/plugins/woocommerce-products-slider/

- https://en-ca.wordpress.org/plugins/woocommerce-gateway-stripe/

- https://wordpress.org/plugins/woocommerce-pdf-invoices-packing-slips/

- https://wordpress.org/plugins/yith-woocommerce-quick-view/ 

- https://wordpress.org/plugins/yith-woocommerce-wishlist/

- https://wordpress.org/plugins/yith-woocommerce-zoom-magnifier/


### Helpful Links

- https://gitlab.com/wpezsuite/WPezToolbox

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader

- https://torquemag.io/2019/12/21-best-woocommerce-plugins-for-your-online-store-most-are-free/


### TODO 



### CHANGE LOG

- v0.0.0 - 6 January 2020
   
   INIT: Hey! Ho!! Let's go!!!


