<?php
/*
Plugin Name: WPezToolbox - Bundle: WooCommerce
Plugin URI: https://gitlab.com/wpezsuite/wpeztoolbox/wpez-toolbox-bundle-woocommerce
Description: WC - A WPezToolbox bundle of WordPress WooCommerce plugins
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleWooCommerce;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__. '\plugins');


function plugins( $arr_in = [] ) {

	$str_bundle           = __( 'WC', 'wpez_tbox' );
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [

		/*
		'X' =>
			[
				'name'     => $str_prefix . 'X',
				'slug'     => 'X',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => '',
						'by_alt' => '',
						'url_by'  => '',
						'desc'    => '',
						'url_img' => '',
					],

					'resources' => [
						'url_wp_org' => '',
						'url_repo' => '',
						'url_site' => '',
						'url_premium' => '',
						'url_fb'   => '',
						'url_tw'   => ''
					]
				]
			],

		*/

		'woocommerce' =>
			[
				'name'     => $str_prefix . 'WooCommerce',
				'slug'     => 'woocommerce',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'WooCommerce / Automattic',
						'url_by'  => 'https://automattic.com/',
						'desc'    => 'WooCommerce is a flexible, open-source eCommerce solution built on WordPress. Whether you’re launching a business, taking an existing brick and mortar store online, or designing sites for clients you can get started quickly and build exactly the store you want.',
						'url_img' => 'https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce/',
						'url_repo' => 'https://github.com/woocommerce/woocommerce',
						'url_site' => 'https://woocommerce.com/',
						'url_premium' => 'https://woocommerce.com/product-category/woocommerce-extensions/',
						'url_fb'   => 'https://www.facebook.com/woocommerce',
						'url_tw'   => 'https://twitter.com/WooCommerce'
					]
				]
			],

		'woocommerce-eu-vat-compliance' =>
			[
				'name'     => $str_prefix . 'EU VAT Compliance Assistant for WooCommerce',
				'slug'     => 'woocommerce-eu-vat-compliance',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'David Anderson',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/davidanderson/',
						'desc'    => 'Since January 1st 2015, all digital goods (including electronic, telecommunications, software, ebook and broadcast services) sold across EU borders have been liable under EU law to EU VAT (a.k.a. IVA) charged in the country of purchase, at the VAT rate in that country. This applies even if the seller is not based in the EU, and there is no minimum threshold. This WooCommerce plugin provides features to assist with EU VAT law compliance.',
						'url_img' => 'https://ps.w.org/woocommerce-eu-vat-compliance/assets/icon-128x128.png?rev=1106140',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-eu-vat-compliance/',
						'url_premium' => 'https://www.simbahosting.co.uk/s3/product/woocommerce-eu-vat-compliance/',
					]
				]
			],

		'woocommerce-direct-checkout' =>
			[
				'name'     => $str_prefix . 'Direct Checkout for WooCommerce',
				'slug'     => 'woocommerce-direct-checkout',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'QuadLayers',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/quadlayers/',
						'desc'    => 'WooCommerce Direct Checkout allows you to simplifies the checkout process by skipping the shopping cart page. The “Add to cart” button is added in each product to redirect customers to the checkout page. This can encourage buyers to shop more and quickly process the transaction, which can lead to a possible increase in sales. This plugin was formerly known as “WooCommerce Direct Checkout”.',
						'url_img' => 'https://ps.w.org/woocommerce-direct-checkout/assets/icon-128x128.png?rev=2064053',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-direct-checkout/',
						'url_site' => 'https://quadlayers.com',
						'url_premium' => 'https://quadlayers.com/portfolio/woocommerce-direct-checkout/',
						'url_fb'   => 'https://www.facebook.com/quadlayers',
						'url_tw'   => 'https://twitter.com/quadlayers'
					]
				]
			],


		'woocommerce-jetpack' =>
			[
				'name'     => $str_prefix . 'Booster for WooCommerce',
				'slug'     => 'woocommerce-jetpack',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Algoritmika Ltd',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/algoritmika/',
						'desc'    => 'Booster for WooCommerce is a WordPress WooCommerce plugin that supercharges your site with awesome powerful features. More than hundred modules. All in one plugin. Features are absolutely required for anyone using excellent WooCommerce platform.',
						'url_img' => 'https://ps.w.org/woocommerce-jetpack/assets/icon-128x128.png?rev=1813426',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-jetpack/',
						'url_repo' => 'https://github.com/algoritmika/woocommerce-jetpack',
						'url_site' => 'https://booster.io/',
						'url_premium' => 'https://booster.io/shop/booster-for-woocommerce-plus-plugin/',
					]
				]
			],


		'rafflepress' =>
			[
				'name'     => $str_prefix . 'RafflePress',
				'slug'     => 'rafflepress',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'RafflePress',
						'url_by'  => 'https://rafflepress.com/',
						'desc'    => 'The Best WordPress Giveaway Plugin. Grow Your Email List, Website Traffic, and Social Media Followers with Viral Giveaways & Contests',
						'url_img' => 'https://ps.w.org/rafflepress/assets/icon-128x128.png?rev=2117711',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/rafflepress/',
						'url_site' => 'https://rafflepress.com/',
						'url_premium' => 'https://rafflepress.com/pricing/',
						'url_fb'   => 'https://facebook.com/rafflepress',
						'url_tw'   => 'https://twitter.com/rafflepress'
					]
				]
			],

		'yith-woocommerce-ajax-search' =>
			[
				'name'     => $str_prefix . 'WooCommerce Ajax Search',
				'slug'     => 'yith-woocommerce-ajax-search',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'YITH',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/yithemes/',
						'desc'    => 'YITH WooCommerce Ajax Search is a very easy-to-use plugin for WooCommerce. You just need to install it and it’s ready to work. It creates a simple search box that shows you instant search results, by suggesting you products from your WooCommerce store that match your searching criteria.',
						'url_img' => 'https://ps.w.org/yith-woocommerce-ajax-search/assets/icon-128x128.jpg?rev=1460948',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/yith-woocommerce-ajax-search/',
						'url_site' => 'https://yithemes.com/',
						'url_premium' => 'https://yithemes.com/themes/plugins/yith-woocommerce-ajax-search/',
						'url_fb'   => 'https://www.facebook.com/YITHplugins',
						'url_tw'   => 'https://twitter.com/yithemes'
					]
				]
			],

		'woocommerce-currency-switcher' =>
			[
				'name'     => $str_prefix . 'Currency Switcher, Multi Currency and Multi Pay for WooCommerce',
				'slug'     => 'woocommerce-currency-switcher',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'RealMag777',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/realmag777/',
						'desc'    => 'WooCommerce Currency Switcher (WOOCS) is multi currency plugin for woocommerce, that allows your site visitors switch products prices currencies according to set currencies rates in the real time and pay in the selected currency (optionally). Allows to add any currency for WooCommerce store. Ideal solution to make the serious WooCommerce store site in multiple currencies!',
						'url_img' => 'https://ps.w.org/woocommerce-currency-switcher/assets/icon-128x128.png?rev=2071512',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-currency-switcher/',
						'url_repo' => 'https://github.com/wp-plugins/woocommerce-currency-switcher',
						'url_site' => 'https://currency-switcher.com/',
						'url_premium' => 'https://codecanyon.net/item/woocommerce-currency-switcher/8085217',
						'url_fb'   => 'https://www.facebook.com/pluginusnet/',
					]
				]
			],

		'woocommerce-menu-bar-cart' =>
			[
				'name'     => $str_prefix . 'WooCommerce Menu Cart',
				'slug'     => 'woocommerce-menu-bar-cart',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Jeremiah Prummer, Ewout Fernhout',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/jprummer/',
						'desc'    => 'Works with WooCommerce, WP-Ecommerce, EDD, Eshop and Jigoshop. This plugin installs a shopping cart button in the navigation bar. The plugin takes less than a minute to setup, and includes various options.',
						'url_img' => 'https://ps.w.org/woocommerce-menu-bar-cart/assets/icon-128x128.png?rev=2190481',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-menu-bar-cart/',
						'url_repo' => 'https://github.com/wpovernight/wp-menu-cart',
						'url_premium' => 'https://wpovernight.com/downloads/menu-cart-pro/',
					]
				]
			],

		'woocommerce-multilingual' =>
			[
				'name'     => $str_prefix . 'WooCommerce Multilingual',
				'slug'     => 'woocommerce-multilingual',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'OnTheGoSystems',
						'url_by'  => 'https://www.onthegosystems.com/',
						'desc'    => 'This ‘glue’ plugin makes it possible to run fully multilingual e-commerce sites using WooCommerce and WPML.',
						'url_img' => 'https://ps.w.org/woocommerce-multilingual/assets/icon-128x128.png?rev=2026021',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-multilingual/',
						'url_repo' => 'https://github.com/wp-premium/woocommerce-multilingual',
						'url_premium' => 'https://wpml.org/purchase/',
						'url_tw'   => 'https://twitter.com/wpml'
					]
				]
			],

		'woocommerce-products-filter' =>
			[
				'name'     => $str_prefix . 'Products Filter for WooCommerce',
				'slug'     => 'woocommerce-products-filter',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'RealMag777',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/realmag777/',
						'desc'    => 'For WooCommerce plugin Products Filter (WOOF) is product search plugin for WooCommerce that allows your site customers filter products by categories, attributes, products tags, products custom taxonomies and price. Supports latest version of the WooCommerce plugin. A must have plugin for your WooCommerce powered online store! Maximum flexibility!',
						'url_img' => 'https://ps.w.org/woocommerce-products-filter/assets/icon-128x128.png?rev=1208072',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-products-filter/',
						'url_repo' => 'https://github.com/wp-plugins/woocommerce-products-filter',
						'url_site' => 'https://products-filter.com/',
						'url_premium' => 'https://codecanyon.net/item/woof-woocommerce-products-filter/11498469',
						'url_fb'   => 'https://www.facebook.com/pluginusnet/',
					]
				]
			],

		'woocommerce-products-slider' =>
			[
				'name'     => $str_prefix . 'PickPlugins Product Slider for WooCommerce',
				'slug'     => 'woocommerce-products-slider',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'PickPlugins',
						'url_by'  => 'https://www.pickplugins.com/',
						'desc'    => 'A premium quality carousel slider to slide your woo-commerce product. easy to use via shortcode anywhere.',
						'url_img' => 'https://ps.w.org/woocommerce-products-slider/assets/icon-128x128.png?rev=1637877',
					],

					'resources' => [
						'url_wp_org' => 'https://en-ca.wordpress.org/plugins/woocommerce-products-slider/',
						'url_premium' => 'https://www.pickplugins.com/item/woocommerce-products-slider-for-wordpress/',
						'url_fb'   => 'https://www.facebook.com/groups/615352905504911/',
					]
				]
			],

		'woocommerce-gateway-stripe' =>
			[
				'name'     => $str_prefix . 'WooCommerce Stripe Payment Gateway',
				'slug'     => 'woocommerce-gateway-stripe',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'WooCommerce / Automattic',
						'url_by'  => 'https://automattic.com/',
						'desc'    => 'Accept Visa, MasterCard, American Express, Discover, JCB, Diners Club, SEPA, Sofort, iDeal, Giropay, Alipay, and more directly on your store with the Stripe payment gateway for WooCommerce, including Apple Pay, Google Pay, and Microsoft Pay for mobile and desktop. The Stripe plugin extends WooCommerce allowing you to take payments directly on your store via Stripe’s API.',
						'url_img' => 'https://ps.w.org/woocommerce-gateway-stripe/assets/icon-128x128.png?rev=1917495',
					],

					'resources' => [
						'url_wp_org' => 'https://en-ca.wordpress.org/plugins/woocommerce-gateway-stripe/',
						'url_repo' => 'https://github.com/woocommerce/woocommerce-gateway-stripe',
						'url_site' => 'https://woocommerce.com/',
						'url_premium' => 'https://woocommerce.com/product-category/woocommerce-extensions/',
						'url_fb'   => 'https://www.facebook.com/woocommerce',
						'url_tw'   => 'https://twitter.com/WooCommerce'
					]
				]
			],

		'woocommerce-pdf-invoices-packing-slips' =>
			[
				'name'     => $str_prefix . 'WooCommerce PDF Invoices & Packing Slips',
				'slug'     => 'woocommerce-pdf-invoices-packing-slips',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Ewout Fernhout',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/pomegranate/',
						'desc'    => 'This WooCommerce extension automatically adds a PDF invoice to the order confirmation emails sent out to your customers. Includes a basic template (additional templates are available from WP Overnight) as well as the possibility to modify/create your own templates. In addition, you can choose to download or print invoices and packing slips from the WooCommerce order admin.',
						'url_img' => 'https://ps.w.org/woocommerce-pdf-invoices-packing-slips/assets/icon-128x128.png?rev=2189942',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-pdf-invoices-packing-slips/',
						'url_repo' => 'https://github.com/wpovernight/woocommerce-pdf-invoices-packing-slips',
						'url_premium' => 'https://wpovernight.com/downloads/woocommerce-pdf-invoices-packing-slips-professional/',
					]
				]
			],


		'yith-woocommerce-quick-view' =>
			[
				'name'     => $str_prefix . 'WooCommerce Quick View',
				'slug'     => 'yith-woocommerce-quick-view',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'YITH',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/yithemes/',
						'desc'    => 'YITH WC Quick View lets users hover their mouse over a button to see a lightbox popup that includes a larger product image, product details, and an add to cart button, without having to leave the product listings or “shop” page.',
						'url_img' => 'https://ps.w.org/yith-woocommerce-quick-view/assets/icon-128x128.jpg?rev=1460911',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/yith-woocommerce-quick-view/',
						'url_site' => 'https://yithemes.com/',
						'url_premium' => 'https://yithemes.com/themes/plugins/yith-woocommerce-quick-view/',
						'url_fb'   => 'https://www.facebook.com/YITHplugins',
						'url_tw'   => 'https://twitter.com/yithemes'
					]
				]
			],

		'yith-woocommerce-wishlist' =>
			[
				'name'     => $str_prefix . 'WooCommerce Wishlist',
				'slug'     => 'yith-woocommerce-wishlist',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'YITH',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/yithemes/',
						'desc'    => 'The wishlist is one of the most powerful and popular tools in an ecommerce shop. Our YITH WooCommerce Wishlist has more than 700,000 active installations and that’s why it’s the most popular wishlist plugin ever.',
						'url_img' => 'https://ps.w.org/yith-woocommerce-wishlist/assets/icon-128x128.jpg?rev=2215573',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/yith-woocommerce-wishlist/',
						'url_site' => 'https://yithemes.com/',
						'url_premium' => 'https://yithemes.com/themes/plugins/yith-woocommerce-wishlist/',
						'url_fb'   => 'https://www.facebook.com/YITHplugins',
						'url_tw'   => 'https://twitter.com/yithemes'
					]
				]
			],

		'yith-woocommerce-zoom-magnifier' =>
			[
				'name'     => $str_prefix . 'WooCommerce Zoom Magnifier',
				'slug'     => 'yith-woocommerce-zoom-magnifier',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'YITH',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/yithemes/',
						'desc'    => 'Offer to your visitors a chance to inspect in detail the quality of your products. With YITH WooCommerce Zoom Magnifier you can add a zoom effect to all your product images. The WordPress plugin also adds a slider below the featured image with your product gallery images.',
						'url_img' => 'https://ps.w.org/yith-woocommerce-zoom-magnifier/assets/icon-128x128.jpg?rev=1461225',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/yith-woocommerce-zoom-magnifier/',
						'url_site' => 'https://yithemes.com/',
						'url_premium' => 'https://yithemes.com/themes/plugins/yith-woocommerce-zoom-magnifier/',
						'url_fb'   => 'https://www.facebook.com/YITHplugins',
						'url_tw'   => 'https://twitter.com/yithemes'
					]
				]
			],



	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}